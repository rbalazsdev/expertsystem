//
//  BRNewsFetcher.m
//  WestpacNews
//
//  Created by Razvan Balazs on 24/10/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import "BRUrlFetcher.h"
#import "BRScenario.h"

@implementation BRUrlFetcher
@synthesize delegate;

- (void)fetchDataForUrl:(NSURL *)urlString {
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:urlString];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest
                                       queue:queue
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data,
                                               NSError *error){
                                                           if ([data length] > 0 && error == nil){
                                                               dispatch_sync(dispatch_get_main_queue(), ^{
                                                               [self.delegate fetcherDidFinish:self withData:[self serializeData:data]];
                                                               });
                                                           }
                                                           else if ([data length] == 0 && error == nil){
                                                               NSLog(@"Nothing downloaded.");
                                                               
                                                           }else if (error) {
                                                               NSLog(@"Download error: %@", [error description]);
                                                               [self.delegate fetcherDidFailWithError:error];
                                                           }
                                               }];
}

//implement this in subclass
- (NSArray *)serializeData:(NSData *)responseData {
    return nil;
}
//not used in this project 
- (NSArray *) sortArray:(NSMutableArray*)array withDescriptorKey:(NSString *)descriptorKey ascending:(BOOL)ascending {
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:descriptorKey ascending:ascending];
    [array sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    return array;
}

@end
