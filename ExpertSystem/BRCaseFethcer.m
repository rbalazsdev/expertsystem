//
//  BRCaseFethcer.m
//  ExpertSystem
//
//  Created by Razvan Balazs on 20/11/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import "BRCaseFethcer.h"
#import "BRCase.h"
#import "BRScenario.h"

@implementation BRCaseFethcer

- (NSArray *)serializeData:(NSData *)responseData {
    NSError *error;
    if (!responseData) {
        return nil;
    }
    NSMutableArray *caseArray = [NSMutableArray arrayWithCapacity:0];
    id jsonObject = [NSJSONSerialization JSONObjectWithData:responseData
                                                    options:kNilOptions
                                                      error:&error];
    
    if ([jsonObject isKindOfClass:[NSDictionary class]]) {
        NSDictionary *caseDict = [jsonObject objectForKey:@"case"];
        BRCase *newCase = [BRCase new];
        [newCase setValuesForKeysWithDictionary:caseDict];
        
        NSMutableArray *answersArray = [NSMutableArray arrayWithCapacity:0];
        for (id dict in newCase.answers) {
            BRScenario *newObject = [[BRScenario alloc] initWithProperties:dict];
            [answersArray addObject:newObject];
        }
        newCase.answers = [NSArray arrayWithArray:answersArray];
        [caseArray addObject:newCase];
    }
    return caseArray;
}

@end
