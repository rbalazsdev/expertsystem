//
//  BRCaseViewController.h
//  ExpertSystem
//
//  Created by Razvan Balazs on 20/11/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BRCaseViewController : UIViewController
@property (nonatomic, assign) NSInteger caseId;

@end
