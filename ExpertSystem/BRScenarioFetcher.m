//
//  BRScenarioFetcher.m
//  ExpertSystem
//
//  Created by Razvan Balazs on 20/11/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import "BRScenarioFetcher.h"
#import "BRScenario.h"

@implementation BRScenarioFetcher

- (NSArray *)serializeData:(NSData *)responseData {
    NSError *error;
    if (!responseData) {
        return nil;
    }
    NSMutableArray *newsArray = [NSMutableArray arrayWithCapacity:0];
    id jsonObject = [NSJSONSerialization JSONObjectWithData:responseData
                                                    options:kNilOptions
                                                      error:&error];
    
    if ([jsonObject isKindOfClass:[NSDictionary class]]) {
        for (id dict in [[(NSDictionary *)jsonObject objectForKey:@"scenarios"] allObjects] ) {
            BRScenario *newObject = [[BRScenario alloc] initWithProperties:dict];
            [newsArray addObject:newObject];
        }
    }
    return newsArray;
}

@end
