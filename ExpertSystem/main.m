//
//  main.m
//  ExpertSystem
//
//  Created by Razvan Balazs on 20/11/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BRAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BRAppDelegate class]));
    }
}
