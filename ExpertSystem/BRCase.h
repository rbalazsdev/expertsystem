//
//  BRCase.h
//  ExpertSystem
//
//  Created by Razvan Balazs on 20/11/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BRCase : NSObject
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, assign) NSInteger id;

@property (nonatomic, strong) NSArray *answers;

- (id)initWithProperties:(NSDictionary *)properties;

@end
