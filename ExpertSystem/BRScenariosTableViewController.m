//
//  BRScenariosTableViewController.m
//  ExpertSystem
//
//  Created by Razvan Balazs on 20/11/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import "BRScenariosTableViewController.h"
#import "BRScenarioFetcher.h"
#import "BRScenario.h"
#import "BRCaseViewController.h"

@interface BRScenariosTableViewController () <BRUrlFetcherDelegate>
@property (nonatomic, strong) BRScenarioFetcher *scenarioFetcher;
@property (nonatomic, strong) NSArray *scenariosArray;

@end

@implementation BRScenariosTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Ask our system:";
    
    self.scenarioFetcher = [BRScenarioFetcher new];
    self.scenarioFetcher.delegate = self;
    
    self.scenariosArray = [[NSArray alloc] init];
    NSString *requestUrl = [INGESTURL stringByAppendingString:@"scenarios/"];
    [self.scenarioFetcher fetchDataForUrl:[NSURL URLWithString:requestUrl]];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [self.scenariosArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    BRScenario *scenario = [self.scenariosArray objectAtIndex:indexPath.row];
    cell.textLabel.text = scenario.text;
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld", (long)scenario.caseId];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"pushCaseSegue" sender:[self.tableView cellForRowAtIndexPath:indexPath]];
}

#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:@"pushCaseSegue"]){
        BRCaseViewController *caseVc = (BRCaseViewController *)segue.destinationViewController;
        NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
        BRScenario *scenario = [self.scenariosArray objectAtIndex:selectedIndexPath.row];
        caseVc.caseId = scenario.caseId;
    }

}


#pragma mark - BRScenarioFetcher delegate methods
- (void)fetcherDidFinish:(BRUrlFetcher *)sender withData:(NSArray *)newsArray{
    self.scenariosArray = [NSArray arrayWithArray:newsArray];
    
    [self.tableView reloadData];

}
- (void)fetcherDidFailWithError:(NSError*)error{
    //nothing to do here at this moment
}

@end
