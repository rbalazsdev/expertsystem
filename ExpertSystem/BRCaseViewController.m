//
//  BRCaseViewController.m
//  ExpertSystem
//
//  Created by Razvan Balazs on 20/11/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import "BRCaseViewController.h"
#import "BRCase.h"
#import "BRCaseFethcer.h"
#import "BRScenario.h"

@interface BRCaseViewController () <BRUrlFetcherDelegate, UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) BRCaseFethcer *caseFetcher;
@property (nonatomic, strong) BRCase *caseToShow;
@property (strong, nonatomic) IBOutlet UIImageView *caseImageView;
@property (weak, nonatomic) IBOutlet UITableView *answersTableView;
@property (nonatomic, strong) NSArray *answersArray;
@property (weak, nonatomic) IBOutlet UILabel *explanationTextLabel;
@end

@implementation BRCaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Please respond/ take action";
    
    self.caseFetcher = [BRCaseFethcer new];
    self.caseFetcher.delegate = self;
    NSString *requestUrl = [INGESTURL stringByAppendingString:@"cases/"];
    requestUrl = [requestUrl stringByAppendingString:[ NSString stringWithFormat:@"%ld",(long)self.caseId]];
    [self.caseFetcher fetchDataForUrl:[NSURL URLWithString:requestUrl]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - BRUrlFetcher Delegate methods
- (void)fetcherDidFinish:(BRUrlFetcher *)sender withData:(NSArray *)newsArray{
    if ([newsArray count] >0) {
        self.caseToShow = [newsArray firstObject];
    }

    self.explanationTextLabel.text = self.caseToShow.text;
    [self.caseImageView setImage:[self downloadImageFromUrlString:self.caseToShow.image]];
    self.answersArray = [NSArray arrayWithArray:self.caseToShow.answers];
    [self.answersTableView reloadData];
}

- (void)fetcherDidFailWithError:(NSError*)error{
    
}

#pragma mark - Image Download
- (UIImage *)downloadImageFromUrlString:(NSString *)urlString {
    NSLog(@"image url:%@", urlString);
    __block UIImage *returnImage;
    if (urlString != nil) {
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        returnImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]]];
        });
    }
    return returnImage;
}




#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [self.answersArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    BRScenario *scenario = [self.answersArray objectAtIndex:indexPath.row];
    cell.textLabel.text = scenario.text;
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld", (long)scenario.caseId];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"pushNewCaseSegue" sender:[self.answersTableView cellForRowAtIndexPath:indexPath]];
}

#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:@"pushNewCaseSegue"]){
        BRCaseViewController *caseVc = (BRCaseViewController *)segue.destinationViewController;
        NSIndexPath *selectedIndexPath = [self.answersTableView indexPathForSelectedRow];
        BRScenario *scenario = [self.answersArray objectAtIndex:selectedIndexPath.row];
        caseVc.caseId = scenario.caseId;
        
    }
    
}

@end
