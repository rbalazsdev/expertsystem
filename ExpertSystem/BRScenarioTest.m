//
//  BRScenarioTest.m
//  ExpertSystem
//
//  Created by Razvan Balazs on 20/11/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BRScenario.h"

@interface BRScenarioTest : XCTestCase

@end

@implementation BRScenarioTest

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testScenarioObj
{
    BRScenario *scenario= [[BRScenario alloc] initWithProperties:@{@"text": @"A"}];

    
    XCTAssertEqualObjects([scenario text], @"A");
    
}

@end
