//
//  BRScenario.h
//  ExpertSystem
//
//  Created by Razvan Balazs on 20/11/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BRScenario : NSObject
@property (nonatomic, copy) NSString *text;
@property (nonatomic, assign) NSInteger id;
@property (nonatomic, assign) NSInteger caseId;

- (id)initWithProperties:(NSDictionary *)properties;

@end
