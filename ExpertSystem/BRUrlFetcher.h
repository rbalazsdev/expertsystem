//
//  BRNewsFetcher.h
//  WestpacNews
//
//  Created by Razvan Balazs on 24/10/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//
#define INGESTURL @"http://expert-system.internal.shinyshark.com/"

#import <Foundation/Foundation.h>

@class BRUrlFetcher;

@protocol BRUrlFetcherDelegate <NSObject>

- (void)fetcherDidFinish:(BRUrlFetcher *)sender withData:(NSArray *)newsArray;
- (void)fetcherDidFailWithError:(NSError*)error;

@end

@interface BRUrlFetcher : NSObject
@property (nonatomic, assign) id <BRUrlFetcherDelegate> delegate;

- (void) fetchDataForUrl:(NSURL*)urlString;
@end
