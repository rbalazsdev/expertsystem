//
//  BRCaseTest.m
//  ExpertSystem
//
//  Created by Razvan Balazs on 20/11/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BRCase.h"

@interface BRCaseTest : XCTestCase

@end

@implementation BRCaseTest

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testCaseObj
{
    BRCase *newCase= [[BRCase alloc] init];
    [newCase setValuesForKeysWithDictionary:@{@"text": @"A"}];
    
    
    XCTAssertEqualObjects([newCase text], @"A");
    XCTAssertNil([newCase image]);
}

@end
